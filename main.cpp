#include <iostream>
#include <bitset>

#include <HierarchyGenerators.h>
#include <Typelist.h>

void func( unsigned _mask, const int* _values ) {
    std::cout << "mask=" << std::bitset< 32 >( _mask ) << "\n";
    for( size_t i = 0; i < 31; i++ )
        std::cout << _values[ i ] << ", ";
    std::cout << _values[ 31 ] << std::endl;
}

enum {
    ALPHA, BETA, GAMMA, DELTA, EPSILON
};

template< size_t N, typename T > class Flag;

class Flags {
    int values[32];
    mutable int array[32];
    unsigned mask;

public:
    template< template< size_t, typename > class Flag, size_t N, typename T >
    Flags& operator | ( const Flag< N, T >& _crFlag ) {
        values[ N ] = *reinterpret_cast< const int* >( &_crFlag.Value() );
        mask |= 1 << N;
        return *this;
    }
    const int* Values() const { return values; }
    const int* Array() const {
        for( size_t i = 0, j = 0; i < 32; i++ ) {
            if( mask & (1 << i) )
                array[ j++ ] = values[ i ];
        }
        return array;
    }
    unsigned Mask() const { return mask; }
};

template< size_t N, typename T >
class Flag {
    T value;
public:
    Flag( const T& _crValue ):
        value( _crValue ) {}
    const T& Value() const { return value; }

    template< template< size_t, typename > class Flag, size_t K, typename U >
    Flags operator | ( const Flag< K, U >& _crFlag ) {
        return Flags() | *this | _crFlag;
    }
};

void adapter( const Flags& _crFlags ) {
    func( _crFlags.Mask(), _crFlags.Array() );
}

struct GType {
    short x;
    short y;
};

typedef Flag< 0, int   > Alpha;
typedef Flag< 1, char  > Beta;
typedef Flag< 2, GType > Gamma;
typedef Flag< 3, int   > Delta;
typedef Flag< 4, char  > Epsilon;


int main() {

    GType a = { 34, 45 };
    adapter( Alpha( 333 ) | Delta( 444 ) | Gamma( a ) );
    return 0;
}

